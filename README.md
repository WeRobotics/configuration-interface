# wer-cargo-configuration

## Features

### Implemented

### To be implemented
- wpa_supplicant.conf
- APN 4G, serial etc...
- yaml generic
- yaml onboard
- Auto IP (get URL ?)
- Stop/Start/Resume services (Autossh + appli)
- Shutdown RPI
- Disk usage, temperature, I2C devices, move logs into trash, delete trash
- Download log
- Download/Upload yaml file + mission file
- WIFI AP / STA

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
