const yaml = require('js-yaml');
const fs   = require('fs');
const { exec } = require("child_process");
const disk = require('diskusage');
const os = require('os');

const interface = require('./backend_src/interface.js')

// CTRL+C
process.on("SIGINT", function () {
    process.exit();
});

var onboardLinks = {};

function newClient(client) {
	client.emit("OnboardLinks", onboardLinks);
	console.log("Sended !")
}

function extractConfig(document) {
	if("communications" in document) {
		extractCommunicationLink(document["communications"])
	}

	interface.send("OnboardLinks", onboardLinks);
}

function extractCommunicationLink(comm) {
	var i = 0;
	comm.forEach(comLink => {
		if(!("name" in comLink)) {
			comLink.name = "Link " + i;
		}

		if(!("type" in comLink)) {
			comLink.type = "serial";
		}

		if(!("api" in comLink)) {
			comLink.api = "none";
		}

		if(comLink.type == "serial") {
			if(!("path" in comLink)) {
				comLink.path = "";
			}

			if(!("baudrate" in comLink)) {
				comLink.baudrate = 115200;
			}
		}
		else {
			comLink.path = null;
			comLink.baudrate = null;
		}

		if(comLink.type == "tcp") {
			if(!("ip" in comLink)) {
				comLink.ip = "0.0.0.0";
			}

			if(!("port" in comLink)) {
				comLink.port = 12345;
			}
		}
		else {
			comLink.ip = null;
			comLink.port = null;
		}

		i++;

		onboardLinks[comLink.name] = comLink;
	});
}

function loadOnboardConfig() {
	try {
		const doc = yaml.safeLoad(fs.readFileSync('/home/pi/cargo-onboard/cargo-onboard/build/bin/config.yaml', 'utf8'));
		extractConfig(doc);
		console.log("Config file loaded sucessfully.");
	} catch (e) {
		console.log(e);
	}
}

loadOnboardConfig();

interface.onConnection(newClient);
interface.start();