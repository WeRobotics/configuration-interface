var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"]
	}
});


// default options
//app.use(fileUpload());

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
	res.header('Access-Control-Allow-Credentials', 'true');
	res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
	res.header('Access-Control-Expose-Headers', 'Content-Length');
	res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
	return next();
  });

//http Web page
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('dist'));

//Socket IO
var conncetionCallback = null;

exports.onConnection = function(callback) {
	conncetionCallback = callback;
}

exports.send = function(topic, data) {
	io.emit(topic, data);
}

exports.start = function() {
	io.on('connection', function (socket) {
		console.info(`A user ${socket.id} connected to Socket IO.`);

		if(conncetionCallback != null) {
			conncetionCallback(socket);
		}
	})

	server.listen(8081);
}