import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import SocketIO from 'socket.io-client';
import VueSocketIOExt from 'vue-socket.io-extended';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Highcharts from "highcharts";
import HcSolidGauge from "highcharts/modules/solid-gauge";
import HcMore from 'highcharts/highcharts-more'
import HcExporting from "highcharts/modules/exporting";
import HcExportData from "highcharts/modules/export-data";
import HcAccessibility from "highcharts/modules/accessibility";
import HighchartsVue from 'highcharts-vue'

const socket = SocketIO('http://192.168.1.208:8081')

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'

//import './assets/style.css'

HcMore(Highcharts)
HcSolidGauge(Highcharts)
HcExporting(Highcharts)
HcExportData(Highcharts)
HcAccessibility(Highcharts)

Vue.use(VueSocketIOExt, socket);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(HighchartsVue)

Vue.config.productionTip = false



new Vue({
  render: h => h(App),
}).$mount('#app')